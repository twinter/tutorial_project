# Programming tutorial application

 A simple demonstration of basic programming principles from an in person tutorial with a friend.

 Based on <https://p5js.org>.

 ## Relevant ressources

 - p5 reference: <https://p5js.org/reference/>
 - JavaScript reference: <https://developer.mozilla.org/en-US/docs/Web/JavaScript>
 - p5 examples: <https://p5js.org/examples/>
