const CANVAS_WIDTH = 600
const CANVAS_HEIGHT = CANVAS_WIDTH * 1.5
let pos_x = 0
let pos_y = 25
let speed_x = 2
let speed_y = 5
let accel_x = 0
let accel_y = 1

function setup() {
  createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);
}

function draw() {
  background(128);
  textSize(25)

  let content = "x:" + pos_x + ", y:" + pos_y
  circle(pos_x, pos_y, 50)
  text(content, pos_x, pos_y + 50)

  pos_x += speed_x
  pos_y += speed_y

  if (pos_x >= CANVAS_WIDTH) {
    speed_x = abs(speed_x) * -1
  } else if (pos_x <= 0) {
    speed_x = abs(speed_x)
  } else {
    speed_x += accel_x
  }

  if (pos_y >= CANVAS_HEIGHT) {
    speed_y = abs(speed_y) * -1
  } else if (pos_y <= 0) {
    speed_y = abs(speed_y)
  } else {
    speed_y += accel_y
  }
}
